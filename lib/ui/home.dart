import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final TextEditingController _alturaControl = new TextEditingController();
  final TextEditingController _pesoControl = new TextEditingController();
  double resultado = 0.0;
  String _resultadoFinal = "";
  String _resultadoConsulta = "";
  void _calcularIMC() {
    setState(() {
      double altura = double.parse(_alturaControl.text);
      double peso = double.parse(_pesoControl.text);

      if ((_alturaControl.text.isNotEmpty) &&
          (_pesoControl.text.isNotEmpty || peso > 0)) {
        resultado = peso / (altura * altura);
        if (double.parse(resultado.toStringAsFixed(1)) < 16.0) {
          _resultadoConsulta = "MUITO GRAVE! Seu peso é muito baixo.";
        } else if (double.parse(resultado.toStringAsFixed(1)) >= 16.0 &&
            resultado < 16.99) {
          _resultadoConsulta = "GRAVE! Seu peso é muito baixo.";
        } else if (double.parse(resultado.toStringAsFixed(1)) >= 17.0 &&
            resultado < 18.49) {
          _resultadoConsulta = "Seu peso é muito baixo.";
        } else if (double.parse(resultado.toStringAsFixed(1)) >= 18.5 &&
            resultado < 24.99) {
          _resultadoConsulta = "Peso normal";
        } else if (double.parse(resultado.toStringAsFixed(1)) >= 25 &&
            resultado < 29.99) {
          _resultadoConsulta = "Cuidado! Sobre peso";
        } else if (double.parse(resultado.toStringAsFixed(1)) >= 30.0 &&
            resultado < 34.99) {
          _resultadoConsulta = "Obesidade grau 1";
        } else if (double.parse(resultado.toStringAsFixed(1)) >= 35.0 &&
            resultado <= 39.99) {
          _resultadoConsulta = "Obesidade grau 2";
        } else if (double.parse(resultado.toStringAsFixed(1)) >= 40.0) {
          _resultadoConsulta = "Obesidade mórbida";
        } else {
          _resultadoConsulta = "0.0";
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('IMC'),
          centerTitle: true,
          backgroundColor: Colors.pink,
        ),
        body: Container(
            alignment: Alignment.topCenter,
            child: ListView(
              padding: const EdgeInsets.all(2.0),
              children: <Widget>[
                Image.asset(
                  'assets/imc_logo.png',
                  height: 75.0,
                  width: 75.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    alignment: Alignment.center,
                    color: Colors.grey.shade300,
                    child: Column(children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextField(
                            keyboardType: TextInputType.number,
                            controller: _alturaControl,
                            decoration: InputDecoration(
                                labelText: 'Altura',
                                hintText: '1.80',
                                icon: Icon(Icons.insert_chart))),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextField(
                            keyboardType: TextInputType.number,
                            controller: _pesoControl,
                            decoration: InputDecoration(
                                labelText: 'Peso (kg)',
                                hintText: '70',
                                icon: Icon(Icons.line_weight))),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                            alignment: Alignment.center,
                            child: RaisedButton(
                              onPressed: _calcularIMC,
                              color: Colors.pink,
                              child: Text('Calcular'),
                              textColor: Colors.white,
                            )),
                      )
                    ]),
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      '${resultado.toStringAsFixed(2)}',
                      style: TextStyle(
                          color: Colors.blueAccent,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.italic,
                          fontSize: 19.9),
                    ),
                    Text(
                      '$_resultadoConsulta',
                      style: TextStyle(
                          color: Colors.pinkAccent,
                          fontWeight: FontWeight.w500,
                          fontStyle: FontStyle.italic,
                          fontSize: 19.9),
                    )
                  ],
                )
              ],
            )));
  }
}
